# MONTAR UNIDADE AO INICIAR O SISTEMA

Primeiramente rodar o comando que irá mostrar as unidades e seus UUIDs
```
sudo blkid
```

Abrir em algum editor de texto
```
sudo [gedit|subl] /etc/fstab
```

deve ficar nesse padrão
```
[Device]                  [Mount Point]        [File System Type] [Options] [Dump] [Pass]
UUID="XXXXXXXXXX"          /media/windows       ntfs               defaults  0      0
```
obs:  [LinuxFilesystems](https://help.ubuntu.com/community/LinuxFilesystemsExplained)



# Indicator-SysMonitor
```
sudo add-apt-repository -y ppa:fossfreedom/indicator-sysmonitor &&
sudo apt-get update &&
sudo apt-get install indicator-sysmonitor -y
```

# My Weather Indicator
```
sudo add-apt-repository -y ppa:atareao/atareao &&
sudo apt-get update &&
sudo apt-get install my-weather-indicator -y
```

# Spotify
```
sudo apt-add-repository -y "deb http://repository.spotify.com stable non-free" &&
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 94558F59 &&
sudo apt-get update -qq &&
sudo apt-get install spotify-client -y
```

# Ícones na bandeja
```
sudo apt-get update && 
sudo apt-get install sni-qt:i386 -y
```

# Java8
```
sudo apt-get purge openjdk* &&
sudo add-apt-repository ppa:webupd8team/java -y &&
sudo apt-get update &&
sudo apt-get install oracle-java8-installer -y &&
sudo apt-get install oracle-java8-set-default -y
```

# Google Chrome
```
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - &&
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' &&
sudo apt-get update &&  
sudo apt-get install google-chrome-stable -y
```